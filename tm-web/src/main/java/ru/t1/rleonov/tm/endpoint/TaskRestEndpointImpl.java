package ru.t1.rleonov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.rleonov.tm.api.endpoint.ITaskRestEndpoint;
import ru.t1.rleonov.tm.api.service.dto.ITaskDTOService;
import ru.t1.rleonov.tm.model.dto.TaskDTO;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpointImpl implements ITaskRestEndpoint {

    @Autowired
    private ITaskDTOService taskService;

    @Override
    @GetMapping("/findAll")
    public List<TaskDTO> findAll() {
        return taskService.findAll();
    }

    @Override
    @PostMapping("/save")
    public TaskDTO save(
            @RequestBody TaskDTO task
    ) {
        return taskService.save(task);
    }

    @Override
    @GetMapping("/findById/{id}")
    public TaskDTO findById(
            @PathVariable("id") String id
    ) {
        return taskService.findOneById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @PathVariable("id") String id
    ) {
        return taskService.findOneById(id) != null;
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return taskService.count();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @PathVariable("id") String id
    ) {
        taskService.removeById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(
            @RequestBody TaskDTO task
    ) {
        taskService.remove(task);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll(
            @RequestBody List<TaskDTO> tasks
    ) {
        taskService.clear(tasks);
    }

    @Override
    @PostMapping("/clear")
    public void clear() {
        taskService.clear();
    }

}
