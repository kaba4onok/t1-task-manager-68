package ru.t1.rleonov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.rleonov.tm.model.dto.ProjectDTO;
import java.util.List;

@RequestMapping("/api/projects")
public interface IProjectRestEndpoint {

    @GetMapping("/findAll")
    List<ProjectDTO> findAll();

    @PostMapping("/save")
    ProjectDTO save(
            @RequestBody ProjectDTO project
    );

    @GetMapping("/findById/{id}")
    ProjectDTO findById(
            @PathVariable("id") String id
    );

    @GetMapping("/existsById/{id}")
    boolean existsById(
            @PathVariable("id") String id
    );

    @GetMapping("/count")
    long count();

    @PostMapping("/deleteById/{id}")
    void deleteById(
            @PathVariable("id") String id
    );

    @PostMapping("/delete")
    void delete(
            @RequestBody ProjectDTO project
    );

    @PostMapping("/deleteAll")
    void deleteAll(
            @RequestBody List<ProjectDTO> projects
    );

    @PostMapping("/clear")
    void clear();

}
