package ru.t1.rleonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.rleonov.tm.api.service.dto.ITaskDTOService;
import ru.t1.rleonov.tm.soap.*;

@Endpoint
public class TaskSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public final static String NAMESPACE = "http://rleonov.t1.ru/tm/soap";

    @Autowired
    private ITaskDTOService taskService;

    @ResponsePayload
    @PayloadRoot(localPart = "taskClearRequest", namespace = NAMESPACE)
    public TaskClearResponse taskClear(@RequestPayload final TaskClearRequest request) throws Exception {
        taskService.clear();
        return new TaskClearResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskCountRequest", namespace = NAMESPACE)
    public TaskCountResponse taskCount(@RequestPayload final TaskCountRequest request) throws Exception {
        @NotNull TaskCountResponse response = new TaskCountResponse();
        response.setCount(taskService.count());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteAllRequest", namespace = NAMESPACE)
    public TaskDeleteAllResponse taskDeleteAll(@RequestPayload final TaskDeleteAllRequest request) throws Exception {
        taskService.clear(request.getTasks());
        return new TaskDeleteAllResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse taskDeleteById(@RequestPayload final TaskDeleteByIdRequest request) throws Exception {
        taskService.removeById(request.getId());
        return new TaskDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    public TaskDeleteResponse taskDelete(@RequestPayload final TaskDeleteRequest request) throws Exception {
        taskService.remove(request.getTask());
        return new TaskDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskExistsByIdRequest", namespace = NAMESPACE)
    public TaskExistsByIdResponse taskExistsById(@RequestPayload final TaskExistsByIdRequest request) throws Exception {
        @NotNull TaskExistsByIdResponse response = new TaskExistsByIdResponse();
        response.setExists(taskService.findOneById(request.getId()) != null);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse taskFindAll(@RequestPayload final TaskFindAllRequest request) throws Exception {
        @NotNull TaskFindAllResponse response = new TaskFindAllResponse();
        response.setTasks(taskService.findAll());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse taskFindById(@RequestPayload final TaskFindByIdRequest request) throws Exception {
        @NotNull TaskFindByIdResponse response = new TaskFindByIdResponse();
        response.setTask(taskService.findOneById(request.getId()));
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse taskSaveRequest(@RequestPayload final TaskSaveRequest request) throws Exception {
        @NotNull TaskSaveResponse response = new TaskSaveResponse();
        response.setTask(taskService.save(request.getTask()));
        return response;
    }

}