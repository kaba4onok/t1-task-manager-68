package ru.t1.rleonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.rleonov.tm.api.service.dto.IProjectDTOService;
import ru.t1.rleonov.tm.soap.*;

@Endpoint
public class ProjectSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public final static String NAMESPACE = "http://rleonov.t1.ru/tm/soap";

    @Autowired
    private IProjectDTOService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "projectClearRequest", namespace = NAMESPACE)
    public ProjectClearResponse projectClear(@RequestPayload final ProjectClearRequest request) throws Exception {
        projectService.clear();
        return new ProjectClearResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectCountRequest", namespace = NAMESPACE)
    public ProjectCountResponse projectCount(@RequestPayload final ProjectCountRequest request) throws Exception {
        @NotNull ProjectCountResponse response = new ProjectCountResponse();
        response.setCount(projectService.count());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteAllRequest", namespace = NAMESPACE)
    public ProjectDeleteAllResponse projectDeleteAll(@RequestPayload final ProjectDeleteAllRequest request) throws Exception {
        projectService.clear(request.getProjects());
        return new ProjectDeleteAllResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse projectDeleteById(@RequestPayload final ProjectDeleteByIdRequest request) throws Exception {
        projectService.removeById(request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse projectDelete(@RequestPayload final ProjectDeleteRequest request) throws Exception {
        projectService.remove(request.getProject());
        return new ProjectDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectExistsByIdRequest", namespace = NAMESPACE)
    public ProjectExistsByIdResponse projectExistsById(@RequestPayload final ProjectExistsByIdRequest request) throws Exception {
        @NotNull ProjectExistsByIdResponse response = new ProjectExistsByIdResponse();
        response.setExists(projectService.findOneById(request.getId()) != null);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse projectFindAll(@RequestPayload final ProjectFindAllRequest request) throws Exception {
        @NotNull ProjectFindAllResponse response = new ProjectFindAllResponse();
        response.setProjects(projectService.findAll());
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse projectFindById(@RequestPayload final ProjectFindByIdRequest request) throws Exception {
        @NotNull ProjectFindByIdResponse response = new ProjectFindByIdResponse();
        response.setProject(projectService.findOneById(request.getId()));
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse projectSave(@RequestPayload final ProjectSaveRequest request) throws Exception {
        @NotNull ProjectSaveResponse response = new ProjectSaveResponse();
        response.setProject(projectService.save(request.getProject()));
        return response;
    }

}