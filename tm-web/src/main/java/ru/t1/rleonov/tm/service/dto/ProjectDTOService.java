package ru.t1.rleonov.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.api.service.dto.IProjectDTOService;
import ru.t1.rleonov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.rleonov.tm.exception.field.IdEmptyException;
import ru.t1.rleonov.tm.exception.field.NameEmptyException;
import ru.t1.rleonov.tm.exception.field.StatusEmptyException;
import ru.t1.rleonov.tm.exception.field.UserIdEmptyException;
import ru.t1.rleonov.tm.model.dto.ProjectDTO;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.enumerated.UserSort;
import ru.t1.rleonov.tm.exception.entity.EntityNotFoundException;
import ru.t1.rleonov.tm.repository.dto.ProjectDTORepository;
import java.util.List;

@Service
@NoArgsConstructor
public class ProjectDTOService extends AbstractWebDTOService<ProjectDTO>
        implements IProjectDTOService {

    @NotNull
    @Autowired
    public ProjectDTORepository repository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable ProjectDTO project = repository.findFirstByUserIdAndId(userId, id);
        if (project == null) throw new EntityNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public ProjectDTO removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable ProjectDTO project = repository.findFirstByUserIdAndId(userId, id);
        if (project == null) throw new EntityNotFoundException();
        repository.delete(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable ProjectDTO project = repository.findFirstByUserIdAndId(userId, id);
        if (project == null) throw new EntityNotFoundException();
        project.setStatus(status);
        repository.save(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@Nullable final String userId,
                           @Nullable final UserSort userSort
    ) {
        @Nullable Sort sort = Sort.by(Sort.Direction.ASC, "name");
        if (userSort == null) return repository.findAllByUserId(userId, sort);
        switch (userSort) {
            case BY_STATUS:
                sort = Sort.by(Sort.Direction.ASC, "status");
                break;
            case BY_CREATED:
                sort = Sort.by(Sort.Direction.ASC, "created");
                break;
            default:
                sort = Sort.by(Sort.Direction.ASC, "name");
        }
        return repository.findAllByUserId(userId, sort);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO save(@Nullable final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        return repository.save(project);
    }

}
