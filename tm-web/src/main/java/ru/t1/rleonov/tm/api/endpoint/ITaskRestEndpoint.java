package ru.t1.rleonov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.rleonov.tm.model.dto.TaskDTO;
import java.util.List;

@RequestMapping("/api/tasks")
public interface ITaskRestEndpoint {

    @GetMapping("/findAll")
    List<TaskDTO> findAll();

    @PostMapping("/save")
    TaskDTO save(
            @RequestBody TaskDTO task
    );

    @GetMapping("/findById/{id}")
    TaskDTO findById(
            @PathVariable("id") String id
    );

    @GetMapping("/existsById/{id}")
    boolean existsById(
            @PathVariable("id") String id
    );

    @GetMapping("/count")
    long count();

    @PostMapping("/deleteById/{id}")
    void deleteById(
            @PathVariable("id") String id
    );

    @PostMapping("/delete")
    void delete(
            @RequestBody TaskDTO task
    );

    @PostMapping("/deleteAll")
    void deleteAll(
            @RequestBody List<TaskDTO> tasks
    );

    @PostMapping("/clear")
    void clear();

}
