package ru.t1.rleonov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.rleonov.tm.api.service.web.IProjectService;
import ru.t1.rleonov.tm.api.service.web.ITaskService;

@Controller
public class TasksController {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @GetMapping("/tasks")
    public ModelAndView index() {
        @NotNull ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskService.findAll());
        modelAndView.addObject("projectRepository", projectService);
        return modelAndView;
    }

}
