package ru.t1.rleonov.tm.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.rleonov.tm.enumerated.Status;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "project")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "projectDTO", propOrder = {
        "id",
        "userId",
        "name",
        "description",
        "status",
        "created"
})

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDTO {

    @Id
    @NotNull
    @XmlElement(required = true)
    private String id = UUID.randomUUID().toString();

    @Nullable
    @XmlElement(required = true)
    @Column(nullable = true, name = "user_id")
    private String userId;

    @Column
    @NotNull
    @XmlElement(required = true)
    private String name = "";

    @Column
    @NotNull
    @XmlElement(required = true)
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    @XmlElement(required = true)
    private Status status = Status.NOT_STARTED;

    @Column
    @NotNull
    @XmlElement(required = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

    @NotNull
    @Override
    public String toString() {
        return name + " : " + status + " : " + description;
    }

    public ProjectDTO(@NotNull final String name) {
        this.name = name;
    }

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.name = name;
        this.description = description;
    }

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final Status status
    ) {
        this.name = name;
        this.status = status;
    }

}
