package ru.t1.rleonov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.rleonov.tm.api.endpoint.IUserEndpoint;
import ru.t1.rleonov.tm.api.service.dto.IUserDTOService;
import ru.t1.rleonov.tm.dto.request.*;
import ru.t1.rleonov.tm.dto.response.*;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.dto.model.SessionDTO;
import ru.t1.rleonov.tm.dto.model.UserDTO;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.rleonov.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    @Autowired
    private IUserDTOService userDTOService;

    @Override
    @NotNull
    @WebMethod
    public UserChangePasswordProfileResponse changePassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordProfileRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();
        @Nullable final UserDTO user = userDTOService.setPassword(userId, password);
        return new UserChangePasswordProfileResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final UserDTO user = userDTOService.lockUserByLogin(login);
        return new UserLockResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final UserDTO user = userDTOService.unlockUserByLogin(login);
        return new UserUnlockResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final UserDTO user = userDTOService.removeByLogin(login);
        return new UserRemoveResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserRegistryResponse registryUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRegistryRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @Nullable final UserDTO user = userDTOService.create(login, password, email);
        return new UserRegistryResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserUpdateProfileResponse updateUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateProfileRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable final UserDTO user = userDTOService.updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(user);
    }

    @Override
    @NotNull
    @WebMethod
    public UserViewProfileResponse viewUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserViewProfileRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDTO user = userDTOService.findOneById(userId);
        return new UserViewProfileResponse(user);
    }

}
