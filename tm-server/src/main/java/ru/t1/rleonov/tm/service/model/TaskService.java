package ru.t1.rleonov.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.api.service.model.ITaskService;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.enumerated.UserSort;
import ru.t1.rleonov.tm.exception.entity.EntityNotFoundException;
import ru.t1.rleonov.tm.exception.field.*;
import ru.t1.rleonov.tm.model.Task;
import ru.t1.rleonov.tm.repository.model.TaskRepository;
import ru.t1.rleonov.tm.repository.model.UserRepository;
import java.util.List;

@Service
@NoArgsConstructor
public class TaskService extends AbstractUserOwnedService<Task>
        implements ITaskService {

    @NotNull
    @Autowired
    public TaskRepository repository;

    @NotNull
    @Autowired
    public UserRepository userRepository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = new Task();
        task.setUser(userRepository.findById(userId).get());
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable Task task = repository.findFirstByUserIdAndId(userId, id);
        if (task == null) throw new EntityNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public Task removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Task task = repository.findFirstByUserIdAndId(userId, id);
        if (task == null) throw new EntityNotFoundException();
        repository.delete(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable Task task = repository.findFirstByUserIdAndId(userId, id);
        if (task == null) throw new EntityNotFoundException();
        task.setStatus(status);
        repository.save(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId,
                                 @Nullable final UserSort userSort
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<Task> models;
        @Nullable Sort sort = Sort.by(Sort.Direction.ASC, "name");
        if (userSort == null) return repository.findAllByUserId(userId, sort);
        switch (userSort) {
            case BY_STATUS:
                sort = Sort.by(Sort.Direction.ASC, "status");
                break;
            case BY_CREATED:
                sort = Sort.by(Sort.Direction.ASC, "created");
                break;
            default:
                sort = Sort.by(Sort.Direction.ASC, "name");
        }
        return repository.findAllByUserId(userId, sort);
    }

}
