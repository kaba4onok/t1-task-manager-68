package ru.t1.rleonov.tm.service.model;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import ru.t1.rleonov.tm.api.service.model.IService;
import ru.t1.rleonov.tm.model.AbstractModel;

@Service
@NoArgsConstructor
public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

}
