package ru.t1.rleonov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.dto.request.TaskStartByIdRequest;
import ru.t1.rleonov.tm.event.ConsoleEvent;
import ru.t1.rleonov.tm.util.TerminalUtil;

@Component
public final class TaskStartByIdListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-start-by-id";

    @NotNull
    private static final String DESCRIPTION = "Start task by id.";

    @Override
    @EventListener(condition = "@taskStartByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(getToken());
        request.setId(id);
        getTaskEndpoint().startTaskById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
